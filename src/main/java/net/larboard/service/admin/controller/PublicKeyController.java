package net.larboard.service.admin.controller;

import net.larboard.service.admin.authentication.service.AuthenticationTokenService;
import net.larboard.service.admin.data.PublicKeyData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public")
public class PublicKeyController {
    private AuthenticationTokenService authenticationTokenService;

    @Autowired
    public PublicKeyController(AuthenticationTokenService authenticationTokenService) {
        this.authenticationTokenService = authenticationTokenService;
    }

    @GetMapping
    public ResponseEntity<PublicKeyData> getPublicTokenKey() {
        return new ResponseEntity<>(authenticationTokenService.getPublicKeyData(), HttpStatus.OK);
    }
}
