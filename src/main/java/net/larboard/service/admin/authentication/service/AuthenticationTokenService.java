package net.larboard.service.admin.authentication.service;

import lombok.extern.slf4j.Slf4j;
import net.larboard.service.admin.authentication.config.AuthConfig;
import net.larboard.service.admin.data.PublicKeyData;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.*;
import java.time.Duration;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;

import static io.jsonwebtoken.SignatureAlgorithm.RS256;

@Service
@Slf4j
public class AuthenticationTokenService {
    private static Base64.Encoder encoder = Base64.getEncoder();

    private final Duration adminJwtValidityDuration;
    private final Duration adminKeypairValidityDuration;
    private final String adminJwtIssuer;

    private PrivateKey privateKey;
    private PublicKeyData publicKeyData;

    private static final KeyPairGenerator keyGen;
    static {
        try {
            keyGen = KeyPairGenerator.getInstance("RSA");

            // Initialize KeyPairGenerator.
            keyGen.initialize(2048);
        }
        catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
            throw new ExceptionInInitializerError(e);
        }
    }

    @Autowired
    public AuthenticationTokenService(AuthConfig authConfig) {
        adminJwtValidityDuration = authConfig.getAdminJwtValidityDuration();
        adminKeypairValidityDuration = authConfig.getAdminKeypairValidityDuration();
        adminJwtIssuer = authConfig.getAdminJwtIssuer();

        createKeypair();
    }

    public String getAuthToken() {
        checkKeypair();

        Instant now = Instant.now();
        return Jwts.builder()
                .setIssuer(adminJwtIssuer)
                .setIssuedAt(Date.from(now))
                .setNotBefore(Date.from(now))
                .setExpiration(Date.from(now.plus(adminJwtValidityDuration)))
                .signWith(RS256, privateKey)
                .compact();
    }

    private synchronized void checkKeypair() {
        if(publicKeyData.getExpiryTime() <= Instant.now().getEpochSecond()) {
            createKeypair();
        }
    }

    private synchronized void createKeypair() {
        // Generate Key Pairs, a private key and a public key.
        KeyPair keyPair = keyGen.generateKeyPair();

        privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        publicKeyData = new PublicKeyData(encoder.encodeToString(publicKey.getEncoded()), Instant.now().getEpochSecond()+adminKeypairValidityDuration.getSeconds());

    }

    public PublicKeyData getPublicKeyData() {
        return publicKeyData;
    }
}
