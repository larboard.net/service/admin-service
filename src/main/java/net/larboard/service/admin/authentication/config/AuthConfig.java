package net.larboard.service.admin.authentication.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Validated
@Configuration
@ConfigurationProperties("application.authentication")
@Getter
@Setter
public class AuthConfig {
    @NotNull
    private Duration adminKeypairValidityDuration = Duration.of(86400L, ChronoUnit.SECONDS);

    @NotNull
    private Duration adminJwtValidityDuration = Duration.of(1L, ChronoUnit.SECONDS);

    @NotNull
    private String adminJwtIssuer;

    @NotNull
    private String adminPassword;

    @NotNull
    private String clientPassword;

    public void setAdminJwtValidityDuration(long jwtAdminValidityDuration) {
        this.adminJwtValidityDuration = Duration.of(jwtAdminValidityDuration, ChronoUnit.SECONDS);
    }

    public void setAdminKeypairValidityDuration(long adminKeypairValidityDuration) {
        this.adminKeypairValidityDuration = Duration.of(adminKeypairValidityDuration, ChronoUnit.SECONDS);
    }
}
